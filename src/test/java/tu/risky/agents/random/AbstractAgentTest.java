package tu.risky.agents.random;

import at.ac.tuwien.ifs.sge.engine.cli.SgeCommand;

public abstract class AbstractAgentTest {
    protected String board = "";
    protected SgeCommand sge = new SgeCommand();

    void loadBoard(String name) {
        board = sge.interpretBoardString(name);
    }
}
