package tu.risky.cli;

import at.ac.tuwien.ifs.sge.engine.cli.SgeCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class CliTest {
    public static void main(String[] args) {
        var agents = new String[]{"TURiskyRiskAbAgent", "RandomAgent"};
        var arguments = new ArrayList<String>();
        arguments.add("match");
        arguments.add("--file=grassauer-pss-risk-env/sge-risk-1.0.1-exe.jar");
        arguments.add("--directory=grassauer-pss-risk-env/agents");
        arguments.add("--debug");
        arguments.add("-c=5");
//        arguments.add("-b=grassauer-pss-risk-env/boards/risk_medium.yaml");
//        arguments.add("-b=grassauer-pss-risk-env/boards/risk_simple_3.yaml");
        arguments.add("-a");
        arguments.addAll(Arrays.stream(agents).collect(Collectors.toList()));
        SgeCommand.main(arguments.toArray(new String[0]));
    }

}
