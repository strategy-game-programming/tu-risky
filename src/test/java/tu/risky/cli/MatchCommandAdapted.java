package tu.risky.cli;

import at.ac.tuwien.ifs.sge.engine.cli.AbstractCommand;
import at.ac.tuwien.ifs.sge.engine.cli.MatchCommand;
import at.ac.tuwien.ifs.sge.engine.cli.SgeCommand;
import at.ac.tuwien.ifs.sge.engine.game.Match;
import at.ac.tuwien.ifs.sge.engine.game.MatchResult;
import at.ac.tuwien.ifs.sge.game.Game;
import at.ac.tuwien.ifs.sge.agent.GameAgent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParentCommand;

@Command(name = "matchadapted", aliases = {
        "ma"}, description = "Let agents play against each other in a single match.")
public class MatchCommandAdapted extends MatchCommand {

    @Option(names = {"-c",
            "--computation-time"}, defaultValue = "60", description = "Amount of computational time given for each action.")
    long computationTime = 60;

    @Option(names = {"-u",
            "--time-unit"}, defaultValue = "SECONDS", description = "Time unit in which -c is. Valid values: ${COMPLETION-CANDIDATES}")
    TimeUnit timeUnit = TimeUnit.SECONDS;

    @ParentCommand
    private SgeCommandAdapted sge;

    @Option(names = {"-p",
            "--number-of-players"}, arity = "1", paramLabel = "N", description = "Number of players. By default the minimum required to play.")
    private int numberOfPlayers = (-1);

    @Option(names = {"-b",
            "--board"}, arity = "1", paramLabel = "BOARD", description = "Optional: Initial board position. Can either be a file or a string.")
    private String board = null;

    @Option(names = {"-r", "-s", "--shuffle"}, description = "Shuffle configuration of agents.")
    private boolean shuffle = false;

    @Option(names = {"-a",
            "--agent"}, arity = "1..*", paramLabel = "AGENT", description = "Configuration of agents.")
    private List<String> agentConfiguration = new ArrayList<>();

    @Override
    public void run() {
        loadCommon();

        if (numberOfPlayers < 0) {
            numberOfPlayers = sge.gameFactory.getMinimumNumberOfPlayers();
        } else if (!(sge.gameFactory.getMinimumNumberOfPlayers() <= numberOfPlayers
                && numberOfPlayers <= sge.gameFactory.getMaximumNumberOfPlayers())) {
            sge.log.error("Game cannot be played with this number of players.");
            throw new IllegalArgumentException("Illegal player number.");
        }

        if (agentConfiguration.size() < numberOfPlayers) {
            sge.fillAgentList(agentConfiguration, numberOfPlayers);
        } else if (agentConfiguration.size() > numberOfPlayers) {
            sge.log.error("Too many agents given.");
            throw new IllegalArgumentException("Illegal agent configuration.");
        }

        if (shuffle) {
            Collections.shuffle(agentConfiguration);
        }

        printAgentConfiguration();

        List<GameAgent<Game<Object, Object>, Object>> agentList = sge
                .createAgentListFromConfiguration(agentConfiguration);

        Match<Game<Object, Object>, GameAgent<Game<Object, Object>, Object>, Object> match = new Match<>(
                sge.gameFactory.newInstance(board, numberOfPlayers),
                agentList, computationTime, timeUnit, sge.debug, sge.log, sge.pool);

        MatchResult<Game<Object, Object>, GameAgent<Game<Object, Object>, Object>> matchResult = match
                .call();

        System.out.println("\n".concat(matchResult.toString()).concat("\n"));
        System.out.println("HENLO");

        destroyAgents(agentList);
    }
}
