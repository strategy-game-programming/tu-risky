package tu.risky.game;

import at.ac.tuwien.ifs.sge.agent.GameAgent;
import at.ac.tuwien.ifs.sge.game.Game;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * This interface clears the screen at every action!
 * ONLY USE THIS FOR Testing purposes
 * @param <G>
 * @param <A>
 */
public interface ClearingGame<G extends Game<A, ?>, A> extends GameAgent<G, A> {
    @Override
    default A computeNextAction(G game, long computationTime, TimeUnit timeUnit) {
        try {
            Runtime.getRuntime().exec("clear");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return processNextAction(game, computationTime, timeUnit);
    }

    A processNextAction(G game, long computationTime, TimeUnit timeUnit);
}
