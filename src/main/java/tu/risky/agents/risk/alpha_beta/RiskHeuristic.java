package tu.risky.agents.risk.alpha_beta;

import at.ac.tuwien.ifs.sge.engine.Logger;
import at.ac.tuwien.ifs.sge.game.Game;
import at.ac.tuwien.ifs.sge.game.risk.board.RiskAction;
import at.ac.tuwien.ifs.sge.game.risk.board.RiskBoard;
import at.ac.tuwien.ifs.sge.game.risk.board.RiskCard;
import at.ac.tuwien.ifs.sge.game.risk.board.RiskTerritory;

import java.io.*;
import java.util.Collection;
import java.util.*;
import java.util.stream.Collectors;

public class RiskHeuristic {

    public static final boolean LOG_ACTIONS = false;
    private static final boolean LOG_WEIGHTS = false;
    public static final boolean SAVE_OUTPUT = false;
    private static final String WEIGHTS_PATH = "./Weights";
    public static final String OUT_PATH = "./out/runs";
    public long timeStamp = System.currentTimeMillis();

    /************************
     * weights
     ************************/
    private double OCCUPY_TERRITORIES_WEIGHT = 300;
    private double TROOP_STRENGTH_WEIGHT = 30;
    private double FRONT_LINE_WEIGHT = 100;
    private double ESTIMATED_REINFORCEMENT_WEIGHT = 30;
    private double CONTINENT_SAFETY_WEIGHT = 10;
    private double CONTINENT_THREAT_WEIGHT = 10;
    private double ENEMY_OCCUPIED_CONTINENTS_WEIGHT = 25;
    private double OWN_OCCUPIED_CONTINENTS_WEIGHT = 60;
    private double CONTINENT_ARMY_DOMINATION_WEIGHT = 10;
    private double CONTINENT_DOMINATION_WEIGHT = 10;
    private double MORE_THAN_ONE_ARMY_WEIGHT = 50;
    private double HINTERLAND_WEIGHT = 50; // small boarder
    private double CARD_WEIGHT = 10;
    private double OCCUPIED_CARD_WEIGHT = 10;
    private double TROOP_DISTRIBUTION_WEIGHT = 0;
    private double TROOP_SPREAD_WEIGHT = 10;
    private double TROOP_SPREAD_QUADRATIC_WEIGHT = 100;
    private double TROOP_FRONT_LINE_DISTRIBUTION_WIGHT = 50;
    private double REINFORCE_FRONT_LINE_WEIGHT = 100;
    private double RUSH_FORWARD_WEIGHT = 10; //should only be relevant in later game

    /**
     * current player
     */
    private int playerId;

    /************************
     *  percentage of occupied territories to change the strategy
     ************************/
    private double FIRST_TERRITORY_GOAL = 0.7;
    private double SECOND_TERRITORY_GOAL = 0.85;

    /*************************
     * map based data structures
     ************************/

    /**
     * Key: ContinentID, Value: Set of territories belonging to this continent
     * RiskTerritory is updated
     */
    private Map<Integer, Map<Integer, RiskTerritory>> continentTerritoryMap;

    /**
     * number of territories in the map
     */
    private final int numberOfTerritories;

    /**
     * Key: ContinentID, Value: continentBonus
     */
    private Map<Integer, Integer> continentBonuses;

    /**
     * Key: ContinentId, Value: boarderCount
     */
    private Map<Integer, Integer> continentBoarderCount;

    /**
     * Key: continentId, Value: Continent Rating
     */
    private Map<Integer, Double> continentRatingMap;
    private Double maxContinentRating = 0.0;

    /**
     * Key: TerritoryID, Value: List of Sets of n-hop neighbours (n being the n-th list element)
     * Gives for each territory the neighbouring territories over n hopes (n element of (0,1,2)).
     * 0-hop neighbours are direct neighbours, 1-hop neighbours are one field away, and so on
     */
    private Map<Integer, List<Set<Integer>>> territoryDistances = new HashMap<>();

    /**
     * Creates data structures based on the game board.
     */
    public RiskHeuristic(RiskBoard board, int playerId, Logger logger) {
        if (LOG_WEIGHTS) {
            loadWeightsFromFile();
            var res = getWeightsDefinition();
            logger.debug(res);
        }
        if (SAVE_OUTPUT) {
            writeWeightsFile();
        }

        continentTerritoryMap = getContinentTerritoryMappings(board);

        numberOfTerritories = board.getTerritories().size();
        continentBonuses = board.getContinentIds()
                .stream()
                .collect(Collectors.toMap(it -> it, board::getContinentBonus));
        continentBoarderCount = board.getContinentIds()
                .stream()
                .collect(Collectors.toMap(it -> it,
                        it -> board.getTerritories().entrySet()
                                .stream()
                                .filter(t -> t.getValue().getContinentId() == it)
                                .map(t -> board.neighboringTerritories(t.getKey()))
                                .flatMap(Collection::stream)
                                .distinct()
                                .map(tId -> board.getTerritories().get(tId))
                                .filter(t -> t.getContinentId() != it)
                                .mapToInt(i -> 1)
                                .sum()
                ));
        this.playerId = playerId;

        continentRatingMap = new HashMap<>();
        for (Integer continentId : continentTerritoryMap.keySet()) {
            var continentRating = getContinentRating(continentId);
            if (continentRating > maxContinentRating) {
                maxContinentRating = continentRating;
            }
            continentRatingMap.put(continentId, continentRating);
        }

        initTerritoryDistances(board);
    }

    /**
     * Writes current weight configuration into a file.
     */
    private void writeWeightsFile() {
        try {
            var weightsFile = new File(String.format("%s/%d_weights", OUT_PATH, timeStamp));
            var createdFile = weightsFile.createNewFile();
            if (createdFile) {
                var res = getWeightsDefinition();
                try (var writer = new FileWriter(weightsFile)) {
                    writer.write(res);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Converts weight configuration to String.
     * @return Current weight configuration as String
     */
    private String getWeightsDefinition() {
        var sb = new StringBuilder();
        sb.append("OCCUPY_TERRITORIES_WEIGHT: ").append(OCCUPY_TERRITORIES_WEIGHT).append("\n");
        sb.append("TROOP_STRENGTH_WEIGHT ").append(TROOP_STRENGTH_WEIGHT).append("\n");
        sb.append("FRONT_LINE_WEIGHT ").append(FRONT_LINE_WEIGHT).append("\n");
        sb.append("ESTIMATED_REINFORCEMENT_WEIGHT ").append(ESTIMATED_REINFORCEMENT_WEIGHT).append("\n");
        sb.append("CONTINENT_SAFETY_WEIGHT ").append(CONTINENT_SAFETY_WEIGHT).append("\n");
        sb.append("CONTINENT_THREAT_WEIGHT ").append(CONTINENT_THREAT_WEIGHT).append("\n");
        sb.append("ENEMY_OCCUPIED_CONTINENTS_WEIGHT ").append(ENEMY_OCCUPIED_CONTINENTS_WEIGHT).append("\n");
        sb.append("OWN_OCCUPIED_CONTINENTS_WEIGHT ").append(OWN_OCCUPIED_CONTINENTS_WEIGHT).append("\n");
        sb.append("CONTINENT_ARMY_DOMINATION_WEIGHT ").append(CONTINENT_ARMY_DOMINATION_WEIGHT).append("\n");
        sb.append("CONTINENT_DOMINATION_WEIGHT ").append(CONTINENT_DOMINATION_WEIGHT).append("\n");
        sb.append("MORE_THAN_ONE_ARMY_WEIGHT ").append(MORE_THAN_ONE_ARMY_WEIGHT).append("\n");
        sb.append("HINTERLAND_WEIGHT ").append(HINTERLAND_WEIGHT).append("\n");
        sb.append("CARD_WEIGHT ").append(CARD_WEIGHT).append("\n");
        sb.append("OCCUPIED_CARD_WEIGHT ").append(OCCUPIED_CARD_WEIGHT).append("\n");
        sb.append("TROOP_DISTRIBUTION_WEIGHT ").append(TROOP_DISTRIBUTION_WEIGHT).append("\n");
        sb.append("TROOP_SPREAD_WEIGHT ").append(TROOP_SPREAD_WEIGHT).append("\n");
        sb.append("TROOP_SPREAD_QUADRATIC_WEIGHT ").append(TROOP_SPREAD_QUADRATIC_WEIGHT).append("\n");
        sb.append("TROOP_FRONT_LINE_DISTRIBUTION_WEIGHT ").append(TROOP_FRONT_LINE_DISTRIBUTION_WIGHT).append("\n");
        sb.append("REINFORCE_FRONT_LINE_WEIGHT ").append(REINFORCE_FRONT_LINE_WEIGHT).append("\n");
        sb.append("RUSH_FORWARD_WEIGHT ").append(RUSH_FORWARD_WEIGHT).append("\n");
        return sb.toString();
    }

    /**
     * Reads weights form an input file and sets the weights
     */
    private void loadWeightsFromFile() {
        var weigthsFile = new File(WEIGHTS_PATH);
        if (weigthsFile.exists()) {
            try {
                try (var reader = new BufferedReader(new InputStreamReader(new FileInputStream(weigthsFile)))) {
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        var weightAssignment = line.replace(" ", "").split("=");
                        var weightName = weightAssignment[0];
                        var weight = weightAssignment[1];
                        var field = this.getClass().getDeclaredField(weightName);
                        field.setAccessible(true);
                        field.set(this, Double.valueOf(weight));
                    }
                }
            } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
                // not probable since we check existance beforehand
            }
        }
    }

    private Map<Integer, Map<Integer, RiskTerritory>> getContinentTerritoryMappings(RiskBoard board) {
        var localContinentTerritoryMap = new HashMap<Integer, Map<Integer, RiskTerritory>>();
        for (Integer territoryId : board.getTerritories().keySet()) {
            var territory = board.getTerritories().get(territoryId);
            localContinentTerritoryMap.putIfAbsent(territory.getContinentId(), new HashMap<>());
            var continentMap = localContinentTerritoryMap.get(territory.getContinentId());
            continentMap.put(territoryId, territory);
        }
        return localContinentTerritoryMap;
    }


    /************************
     * Heuristics
     ************************/

    /**
     * Returns a negative reward if a field stacks too many troops (ownTroops vs neighbouring enemy troops)
     */
    private double calculateTroopDistributionHeuristic(RiskBoard board) {
        var safeties = new HashMap<Integer, Double>();
        var maxSafety = 0.0;
        var territories = board.getTerritories();
        for (Integer territoryId : territories.keySet()) {
            var territory = territories.get(territoryId);
            if (territory.getOccupantPlayerId() == playerId) {
                var safety = getSafety(board, territoryId);
                safeties.put(territoryId, safety);
                if (safety > maxSafety) {
                    maxSafety = safety;
                }
            }
        }
        var cap = 10 * maxSafety;
        var total = 0.0;
        for (Double safety : safeties.values()) {
            total += safety * safety;
        }
        total += maxSafety;
        if (total > cap) {
            total = cap;
        }
        if (cap == 0) { cap = 1;
        }
        return -1 * total / cap;
    }

    /**
     * Put reinforcements along front line.
     * @param game game state
     * @return heuristic value in [0,1]
     */
    private double calculateReinforceFrontLineHeuristic(Game<RiskAction, RiskBoard> game){

        var board = game.getBoard();
        var gamePhase = getGamePhase(board);
        if(gamePhase != GamePhase.REINFORCEMENT_PHASE){
            return 0;
        }
        var action= game.getPreviousAction();
        var reinforcedTerritory = action.reinforcedId();
        var reinforcementTroops = action.troops();
        if(reinforcementTroops <= 0){
            return 0;
        }
        if(board.isTerritory(reinforcedTerritory)) {
            var enemyTroops = board.neighboringEnemyTerritories(reinforcedTerritory)
                    .stream()
                    .mapToInt(territoryId -> game.getBoard().getTerritoryTroops(territoryId))
                    .sum();

            if (reinforcementTroops >= enemyTroops) {
                return 1;
            }
            return enemyTroops / (double)reinforcementTroops;
        }
        return 0;
    }

    /**
     * Measurement of how likely it is to loose a fully occupied continent.
     * @param board game state
     * @return heuristic value in [-1,0]
     */
    private double calculateContinentSafetyHeuristic(RiskBoard board) {
        var continentSafetyFeature = 0.0;
        for (Integer continentId : continentTerritoryMap.keySet()) {
            var territoryThreats = new HashMap<Integer, Double>();
            var currentTerritories = continentTerritoryMap.get(continentId);
            for (Integer territoryId : currentTerritories.keySet()) {
                var territory = currentTerritories.get(territoryId);
                if (territory.getOccupantPlayerId() != playerId) {
                    territoryThreats.clear();
                    break; // there cannot be a continent safety if you don't own the continent
                }
                var threat = getThreat(board, territoryId);
                if (threat != 0.0) {
                    territoryThreats.put(territoryId, threat);
                }
            }
            var totalThreat = 0.0;
            for (var threat : territoryThreats.values()) {
                totalThreat += threat * threat; // sum threat(b)²
            }
            var maxThreat = territoryThreats.values().stream().mapToDouble(it -> it).max().orElse(0.0);
            var threatCap = maxThreat * 10;
            var summedThreat = (totalThreat + maxThreat);
            if (summedThreat > threatCap) {
                summedThreat = threatCap;
            }
            if (threatCap == 0) {
                threatCap = 1;
            }
            var normalizedThreat = summedThreat / threatCap;
            continentSafetyFeature += -1 * normalizedThreat * calculateNormalizedContinentRating(continentId);
            territoryThreats.clear();
        }
        return continentSafetyFeature / continentTerritoryMap.size();
    }

    /**
     * Returns a positive value on how likely it is to occupy and continent which is fully occupied by the other player.
     * @param board game board
     * @return heuristic value in [0,1]
     */
    private double calculateContinentThreatHeuristic(RiskBoard board) {
        var continentThreatFeature = 0.0;
        for (Integer continentId : continentTerritoryMap.keySet()) {
            var territoryThreats = new HashMap<Integer, Double>();
            var currentTerritories = continentTerritoryMap.get(continentId);
            for (Integer territoryId : currentTerritories.keySet()) {
                var territory = currentTerritories.get(territoryId);
                if (territory.getOccupantPlayerId() == playerId || territory.getOccupantPlayerId() == -1) {
                    territoryThreats.clear();
                    break; // there cannot be a continent safety if you don't own the continent
                }
                var threat = getThreat(board, territoryId);
                if (threat != 0.0) {
                    territoryThreats.put(territoryId, threat);
                }
            }
            var totalThreat = 0.0;
            for (var threat : territoryThreats.values()) {
                totalThreat += threat * threat; // sum threat(b)²
            }
            var maxThreat = territoryThreats.values().stream().mapToDouble(it -> it).max().orElse(0.0);
            var threatCap = maxThreat * 10;
            if (totalThreat > threatCap) {
                totalThreat = threatCap;
            }
            if (threatCap == 0) {
                threatCap = 1;
            }
            totalThreat = totalThreat / threatCap;
            continentThreatFeature += totalThreat * calculateNormalizedContinentRating(continentId);
        }
        return continentThreatFeature;
    }

    /**
     * Get threat for a given territory. (Max enemy boarder troops/own troops).
     *
     * @param board game board
     * @param territoryId territory for which threat should be calculated
     * @return less than 1 -> enemy has less troops, greater than 1 -> enemy has more troops
     */
    private Double getThreat(RiskBoard board, Integer territoryId) {
        return board.neighboringEnemyTerritories(territoryId)
                .stream()
                .map(it -> board.getTerritories().get(it))
                .filter(it -> it.getOccupantPlayerId() != playerId)
                .max(Comparator.comparing(RiskTerritory::getTroops))
                .map(riskTerritory -> riskTerritory.getTroops() / (double) board.getTerritories().get(territoryId).getTroops())
                .orElse(0.0);
    }

    /**
     * Get safety for a given territory. (own troops/Max enemy boarder troops).
     *
     * @param board game board
     * @param territoryId territory for which safety should be calculated
     * @return less than 1 -> enemy has more troops ,greater than 1 -> enemy has less troops
     */
    private Double getSafety(RiskBoard board, Integer territoryId) {
        return board.neighboringEnemyTerritories(territoryId)
                .stream()
                .map(it -> board.getTerritories().get(it))
                .filter(it -> it.getOccupantPlayerId() != playerId)
                .max(Comparator.comparing(RiskTerritory::getTroops))
                .map(riskTerritory -> {
                    var troops = riskTerritory.getTroops();
                    if (troops == 0) {
                        troops = 1;
                    }
                    return board.getTerritories().get(territoryId).getTroops() / (double) troops;
                })
                .orElse(0.0);
    }

    /**
     * Calculates for each territory the direct neighbours, and the one-hop, and two-hop neighbours.
     * Sets field {@link RiskHeuristic#territoryDistances}
     *
     * @param board game board
     */
    private void initTerritoryDistances(RiskBoard board) {
        for (var territory : board.getTerritories().entrySet()) {

            List<Set<Integer>> distanceSets = new ArrayList<>();

            Set<Integer> directNeighbours = new HashSet<>(board.neighboringTerritories(territory.getKey()));
            Set<Integer> oneHopNeighbours = new HashSet<>();
            Set<Integer> twoHopNeighbours = new HashSet<>();
            for (var nextNeighbour : directNeighbours) {
                oneHopNeighbours.addAll(board.neighboringTerritories(nextNeighbour));
            }
            oneHopNeighbours.removeAll(directNeighbours);
            for (var nextNeighbour : oneHopNeighbours) {
                twoHopNeighbours.addAll(board.neighboringTerritories(nextNeighbour));
            }
            twoHopNeighbours.removeAll(directNeighbours);
            twoHopNeighbours.removeAll(oneHopNeighbours);
            distanceSets.add(directNeighbours);
            distanceSets.add(oneHopNeighbours);
            distanceSets.add(twoHopNeighbours);
            territoryDistances.put(territory.getKey(), distanceSets);
        }
    }

    /**
     * Returns a negative incentive for each enemy occupied continent (-1 * enemyOccupiedContinents/totalContinents)
     * @return heuristic value in [-1,0]
     */
    private double calculateEnemyOccupiedContinentsHeuristic() {
        var enemyOccupiedContinents = continentTerritoryMap.values()
                .stream()
                .filter(it -> it.values().stream().allMatch(territory -> territory.getOccupantPlayerId() != playerId && territory.getOccupantPlayerId() != -1))
                .count();
        return -1 * enemyOccupiedContinents / (double) continentTerritoryMap.size();
    }

    /**
     * Returns a positive incentive for each occupied continent (ownOccupiedContinents/totalContinents)
     * @return heuristic value in [0,1]
     */
    private double calculateOwnOccupiedContinentsHeuristic() {
        var ownOccupiedContinents = continentTerritoryMap.values()
                .stream()
                .filter(it -> it.values().stream().allMatch(territory -> territory.getOccupantPlayerId() == playerId))
                .count();
        return ownOccupiedContinents / (double) continentTerritoryMap.size();
    }

    /**
     * Returns an incentive for continents where the players aries dominate the enemy players armies
     * @retrun heuristic value in [0,1]
     */
    private double calculateContinentArmyDominationHeuristic(RiskBoard board) {
        var continentArmyDominationFeature = 0.0;
        for (Integer continentId : continentTerritoryMap.keySet()) {
            continentArmyDominationFeature += getContinentArmyRatio(continentId);
        }
        return continentArmyDominationFeature / (double) continentTerritoryMap.size();
    }

    /**
     * Own armies / total armies for the given continent
     * @param continentId continent id
     * @return heuristic value in [0,1]
     */
    private double getContinentArmyRatio(Integer continentId) {
        var totalArmies = 0.0;
        var ownArmies = 0.0;
        var territoryMap = continentTerritoryMap.get(continentId);
        for (RiskTerritory territory : territoryMap.values()) {
            if (territory.getOccupantPlayerId() == playerId) {
                ownArmies += territory.getTroops();
            }
            totalArmies += territory.getTroops();
        }
        if (totalArmies != 0.0) {
            return ownArmies / totalArmies;
        }
        return 0.0;
    }

    /**
     * Returns an incentive for continents where the players territories dominate the enemy players territories
     * @retun heuristic Value in [0,1]
     */
    private double calculateContinentDominationHeuristic() {
        var continentDominationFeature = 0.0;
        for (Integer continentId : continentTerritoryMap.keySet()) {
            var armyRatio = getContinentArmyRatio(continentId);
            var totalTerritories = 0;
            var ownTerritories = 0;
            for (RiskTerritory territory : continentTerritoryMap.get(continentId).values()) {
                if (territory.getOccupantPlayerId() == playerId) {
                    ownTerritories++;
                }
                totalTerritories++;
            }
            if (totalTerritories != 0) {
                continentDominationFeature += ((armyRatio + ownTerritories / (double) totalTerritories) / 2) * calculateNormalizedContinentRating(continentId);
            }
        }
        return continentDominationFeature / (double) continentTerritoryMap.size();
    }

    /**
     * Returns an incentive to keep territories fortified (e.g. > 1 Army) of the given player
     * @return Value in [0,1]
     */
    private double calculateMoreThanOneArmyHeuristic(RiskBoard board) {
        var fortifiedTerritories = 0;
        var toatlTerrirotiers = 0;
        for (RiskTerritory territory : board.getTerritories().values()) {
            if (territory.getOccupantPlayerId() == playerId && territory.getTroops() > 1) {
                fortifiedTerritories++;
            }
            toatlTerrirotiers++;
        }
        if (toatlTerrirotiers != 0) {
            return fortifiedTerritories / (double) toatlTerrirotiers;
        }
        return 0.0;
    }

    /**
     * Calculate utility values
     * @param game game state
     * @return one if we win
     */
    public double calculateUtilityValue(Game<RiskAction, RiskBoard> game) {
        if (game.isGameOver() && game.getBoard().getTerritories().values().stream().findFirst().get().getOccupantPlayerId() == playerId) {
            return 1;
        }
        return 0;
    }

    /**
     * Returns heuristic value based on the conquered continents and the value of each continent.
     *
     * @param game game state
     * @return heuristic vale for continent bonus
     */
    private double calculateContinentRatingForAllContinents(Game<RiskAction, RiskBoard> game) {
        return game.getBoard().getContinentIds()
                .stream()
                .filter(it -> isContinentConquered(it, game.getBoard().getTerritories().values()))
                .mapToDouble(this::getContinentRating)
                .sum();
    }

    /**
     * Calculate a heuristic based on the overall army strength.
     *
     * @param game current came state
     * @return heuristic value in [0,1]
     */
    private double calculateTroopStrengthHeuristic(Game<RiskAction, RiskBoard> game) {
        var board = game.getBoard();
        double troops = 0;
        double enemyTroops = 0;

        for (int pId = 0; pId < board.getNumberOfPlayers(); pId++) {
            for (Integer territoryId : board.getTerritoriesOccupiedByPlayer(pId)) {
                var territoryTroops = board.getTerritoryTroops(territoryId);
                if (pId == this.playerId) {
                    troops += territoryTroops;
                } else {
                    enemyTroops += territoryTroops;
                }
            }
        }
        double totalTroops = troops + enemyTroops;
        if (totalTroops != 0.0) {
            return troops / totalTroops;
        }
        return 0.0;
    }

    /**
     * Troops closer to the front line give a bigger bonus.
     *
     * @param game current game state
     * @return heuristic value in [0,1]
     */
    private double calculateFrontLineHeuristic(Game<RiskAction, RiskBoard> game) {

        var board = game.getBoard();
        if (getGamePhase(board) == GamePhase.ATTACK_PHASE) {
            return 0;
        }

        var enemyPlayerId = playerId == 0 ? 1 : 0;
        double frontLineTroops = 0;
        double weightedFrontLineTroops = 0;
        for (Integer enemyTerritory : board.getTerritoriesOccupiedByPlayer(enemyPlayerId)) {
            for (int hops = 0; hops < 2; hops++) {
                for (var adjacentTerritory : territoryDistances.get(enemyTerritory).get(hops)) {
                    if (board.getTerritories().get(adjacentTerritory).getOccupantPlayerId() == playerId) {
                        var troops = board.getTerritories().get(adjacentTerritory).getTroops();
                        frontLineTroops += troops;
                        weightedFrontLineTroops += troops * (1 + hops * 3);
                    }
                }
            }
        }
        if (weightedFrontLineTroops != 0.0) {
            return frontLineTroops / weightedFrontLineTroops;
        }
        return 0.0;
    }

    /**
     * Rush with all troops to the front line.
     * This is supposed to gain importance in the later game.
     *
     * @param game current game state
     * @return heuristic value in [0,1]
     */
    private double calculateRushForwardHeuristic(Game<RiskAction, RiskBoard> game) {

        var board = game.getBoard();
        if (getGamePhase(board) == GamePhase.ATTACK_PHASE) {
            return 0;
        }

        var enemyPlayerId = playerId == 0 ? 1 : 0;
        double frontLineTroops = 0;
        double backupTroops = 0;
        for (Integer enemyTerritory : board.getTerritoriesOccupiedByPlayer(enemyPlayerId)) {
            for (var adjacentTerritory : territoryDistances.get(enemyTerritory).get(0)) {
                if (board.getTerritories().get(adjacentTerritory).getOccupantPlayerId() == playerId) {
                    var troops = board.getTerritories().get(adjacentTerritory).getTroops();
                    frontLineTroops += troops;
                }
            }
            for (var adjacentTerritory : territoryDistances.get(enemyTerritory).get(1)) {
                if (board.getTerritories().get(adjacentTerritory).getOccupantPlayerId() == playerId) {
                    var troops = board.getTerritories().get(adjacentTerritory).getTroops();
                    backupTroops += troops;
                }
            }
        }
        var relevantTroops = frontLineTroops + backupTroops;
        relevantTroops = (relevantTroops==0) ? 1 : relevantTroops;

        return frontLineTroops/relevantTroops;
    }

    /**
     * Gives better troop distribution along the boarder.
     *
     * @param game current game state
     * @return heuristic value in [0,1]
     */
    private double calculateFrontLineTroopDistribution(Game<RiskAction, RiskBoard> game){
        var board = game.getBoard();

        if(getGamePhase(board) == GamePhase.ATTACK_PHASE){
            return 0;
        }
        double count = 0;
        double distributionFactor = 0;
        for (var territory : board.getTerritoriesOccupiedByPlayer(playerId)) {
            for (var enemyNeighbour : board.neighboringEnemyTerritories(territory)) {
                var smallerForce = Math.min(board.getTerritoryTroops(territory), board.getTerritoryTroops(enemyNeighbour));
                var largerForce = Math.max(board.getTerritoryTroops(territory), board.getTerritoryTroops(enemyNeighbour));
                largerForce = largerForce == 0 ? 1 : largerForce;
                distributionFactor += smallerForce / (double) largerForce;
                count++;
            }
        }
        count = count == 0 ? 1 : count;
        return distributionFactor / count;
    }

    /**
     * Ensures that troops are not stacked on a single field.
     * Gives higher bonus if troops are not stacked.
     *
     * @param game game state
     * @return heuristic value in [0,1]
     */
    private double calculateTroopSpreadHeuristic(Game<RiskAction, RiskBoard> game) {

        var board = game.getBoard();
        if (getGamePhase(board) == GamePhase.ATTACK_PHASE) {
            return 0;
        }

        double count = 0;
        double distributionFactor = 0;
        for (var territory : board.getTerritoriesOccupiedByPlayer(playerId)) {
            for (var friendlyNeighbour : board.neighboringFriendlyTerritories(territory)) {
                var secondaryForce = Math.min(board.getTerritoryTroops(territory), board.getTerritoryTroops(friendlyNeighbour));
                var mainForce = Math.max(board.getTerritoryTroops(territory), board.getTerritoryTroops(friendlyNeighbour));
                mainForce = mainForce == 0 ? 1 : mainForce;
                distributionFactor += secondaryForce / (double) mainForce;
                count++;
            }
        }

        count = count == 0 ? 1 : count;
        return distributionFactor / count;
    }

    /**
     * Ensures that troops are not stacked on a single field.
     * Gives higher bonus if troops are not stacked.
     * Reward function decreases drastically.
     *
     * @param game game state
     * @return heuristic value in [0,1]
     */
    private double calculateTroopSpreadQuadraticHeuristic(Game<RiskAction, RiskBoard> game) {

        var board = game.getBoard();
        if (getGamePhase(board) == GamePhase.ATTACK_PHASE) {
            return 0;
        }

        double count = 0;
        double distributionFactor = 0;
        for (var territory : board.getTerritoriesOccupiedByPlayer(playerId)) {
            for (var friendlyNeighbour : board.neighboringFriendlyTerritories(territory)) {
                var secondaryForce = Math.min(board.getTerritoryTroops(territory), board.getTerritoryTroops(friendlyNeighbour));
                var mainForce = Math.max(board.getTerritoryTroops(territory), board.getTerritoryTroops(friendlyNeighbour));
                mainForce = mainForce == 0 ? 1 : mainForce;
                distributionFactor += secondaryForce / (double) mainForce;
                distributionFactor = Math.pow(distributionFactor, 2);
                count++;
            }
        }

        count = count == 0 ? 1 : count;
        return distributionFactor / count;
    }

    /**
     * Gives a rating how valuable a continent is.
     * Calculated by:
     * 15 + Army Bonus - 4 x Border Number
     * -----------------------------------
     * Territory Number
     *
     * @param continentId id of the continent for which we want the result
     * @return rating of the continent
     */
    private double getContinentRating(Integer continentId) {
        return (15 + continentBonuses.get(continentId) - (4 * continentBoarderCount.get(continentId))) / ((double) continentTerritoryMap.get(continentId).size());
    }

    /**
     * returns the continent rating normalized between [0,1]
     */
    private double calculateNormalizedContinentRating(Integer continentId) {
        return continentRatingMap.get(continentId) / maxContinentRating;
    }

    /**
     * Checks if a continent is completely occupied by the current player.
     *
     * @param continentId id of the continent for which we want the result
     * @param territories all territories in the game
     * @return ture if the current player occupies the whole continent, false otherwise
     */
    private boolean isContinentConquered(Integer continentId, Collection<RiskTerritory> territories) {
        return territories.stream()
                .filter(t -> t.getContinentId() == continentId)
                .allMatch(t -> t.getOccupantPlayerId() == playerId);
    }

    /**
     * Gives the current game phase.
     *
     * @param board game board
     * @return current game phase
     */
    public static GamePhase getGamePhase(RiskBoard board) {
        if (board.isAttackPhase())
            return GamePhase.ATTACK_PHASE;
        else if (board.isFortifyPhase())
            return GamePhase.FORTIFY_PHASE;
        else if (board.isOccupyPhase())
            return GamePhase.OCCUPY_PHASE;
        else if (board.isReinforcementPhase())
            return GamePhase.REINFORCEMENT_PHASE;
        else
            return null;
    }

    /**
     * This heuristic ensures, that the agent keeps the border short.
     *
     * @param game game state
     * @return heuristic value in interval [0,1]
     */
    private double calculateHinterlandHeuristic(Game<RiskAction, RiskBoard> game) {
        RiskBoard board = game.getBoard();

        GamePhase currentPhase = getGamePhase(board);
        if (currentPhase == GamePhase.OCCUPY_PHASE || currentPhase == GamePhase.FORTIFY_PHASE) {
            return 0;
        }

        Set<Integer> frontLine = new HashSet<>();
        for (int territoryId : board.getTerritoriesOccupiedByPlayer(playerId)) {
            if (board.neighboringEnemyTerritories(territoryId).size() > 0) {
                frontLine.add(territoryId);
            }
        }
        int numHinterlandTerritories = board.getNrOfTerritoriesOccupiedByPlayer(playerId) - frontLine.size();
        return (float) numHinterlandTerritories / board.getNrOfTerritoriesOccupiedByPlayer(playerId);
    }

    /**
     * Gives an incentive to control more territories.
     * @param board game board
     * @return value in [0,1]
     */
    private double calculateTerritoriesHeuristic(RiskBoard board) {
        return board.getNrOfTerritoriesOccupiedByPlayer(playerId) / (double) numberOfTerritories;
    }

    /**
     * Gives reward, if we own the territory for which we hold the card.
     *
     * @param game game state
     * @return heuristic value in [0,1]
     */
    private double calculateOccupiedCardHeuristic(Game<RiskAction, RiskBoard> game) {
        RiskBoard board = game.getBoard();
        GamePhase currentPhase = getGamePhase(board);
        if (currentPhase == GamePhase.OCCUPY_PHASE || currentPhase == GamePhase.FORTIFY_PHASE) {
            return 0;
        }

        int occupiedCards = 0;

        for (RiskCard card : board.getPlayerCards(playerId)) {
            if (board.isTerritory(card.getTerritoryId()) && board.getTerritoryOccupantId(card.getTerritoryId()) == playerId) {
                occupiedCards++;
            }
        }
        var playerCardsSize = board.getPlayerCards(playerId).size();
        if (playerCardsSize == 0) {
            playerCardsSize = 1;
        }
        return ((double) occupiedCards) / playerCardsSize;
    }

    /**
     * Heuristic that gives bonus based on the expected armies that will be available in the next move.
     * Value is normalized ReLu-like.
     * Note: trade in bonus is neglected.
     *
     * @param game game state
     * @return heuristic value [0,1]
     */
    private double calculateEstimatedReinforcementHeuristic(Game<RiskAction, RiskBoard> game) {
        GamePhase currentPhase = getGamePhase(game.getBoard());
        var board = game.getBoard();
        if (currentPhase == GamePhase.OCCUPY_PHASE || currentPhase == GamePhase.FORTIFY_PHASE) {
            return 0;
        }

        // troops from occupied territories
        double estimatedReinforcements = 2 * Math.max(Math.floor(game.getHeuristicValue(this.playerId) / 3), 3);
        // troops from continent bonuses
        for (Integer continentId : board.getContinentIds()) {
            if (isContinentConquered(continentId, board.getTerritories().values())) {
                estimatedReinforcements += board.getContinentBonus(continentId);
            }
        }
        return Math.min(1, estimatedReinforcements / 10);
    }


    /**
     * Gives a reward based on the number of cards the player holds.
     * Normalized by number of cards.
     *
     * @param game game state
     * @return heuristic value in [0,1]
     */
    private double calculateCardHeuristic(Game<RiskAction, RiskBoard> game) {
        GamePhase currentPhase = getGamePhase(game.getBoard());
        if (currentPhase == GamePhase.OCCUPY_PHASE || currentPhase == GamePhase.FORTIFY_PHASE) {
            return 0;
        }
        return ((double) game.getBoard().getPlayerCards(playerId).size()) / game.getBoard().getNumberOfCards();
    }

    /**
     * Get the heuristic for the given node
     * @param game game state
     * @return heuristic value for game state
     */
    public double calculateHeuristic(at.ac.tuwien.ifs.sge.game.Game<RiskAction, RiskBoard> game) {

        var board = game.getBoard();
        int numTerritories = board.getTerritories().size();

        /*
         * Adjust weights based on game state
         */
        var occupyTerritoriesWeight = OCCUPY_TERRITORIES_WEIGHT;
        var rushForwardWeight = RUSH_FORWARD_WEIGHT;

        double occupiedTerritories = board.getTerritoriesOccupiedByPlayer(playerId).size();
        if( occupiedTerritories > numTerritories * FIRST_TERRITORY_GOAL ) {
            occupyTerritoriesWeight = 1000;
        }
        if( occupiedTerritories > numTerritories * SECOND_TERRITORY_GOAL ) {
            rushForwardWeight = 100;
            occupyTerritoriesWeight = 10000;
        }

        /*
         * Calculate reward
         */
        double reward = 0;
        continentTerritoryMap = getContinentTerritoryMappings(board);
//        playerId = game.getCurrentPlayer(); // todo test

        /*
         * Occupied Territories Feature
         */
        var occupyTerritoriesHeuristic = occupyTerritoriesWeight * calculateTerritoriesHeuristic(board);
        reward += occupyTerritoriesHeuristic;

        /*
         * Armies Feature
         */
        var troopStrengthHeuristic = TROOP_STRENGTH_WEIGHT * calculateTroopStrengthHeuristic(game);
        reward += troopStrengthHeuristic;


        /*
         * Distance to Frontier Feature
         */
        var frontLineHeuristic = FRONT_LINE_WEIGHT * calculateFrontLineHeuristic(game);
        reward += frontLineHeuristic;


        /*
         * Continent Safety
         */
        var continentSafetyHeuristic = CONTINENT_SAFETY_WEIGHT * calculateContinentSafetyHeuristic(board);
        reward += continentSafetyHeuristic;

        /*
         * Continent Threat
         */
        var continentThreatHeuristic = CONTINENT_THREAT_WEIGHT * calculateContinentThreatHeuristic(board);
        reward += continentThreatHeuristic;

        /*
         * hinterland
         */
        var hinterlandHeuristic = HINTERLAND_WEIGHT * calculateHinterlandHeuristic(game);
        reward += hinterlandHeuristic;

        /*
         * card heuristic
         */
        var cardHeuristic = CARD_WEIGHT * calculateCardHeuristic(game);
        reward += cardHeuristic;

        /*
         * occupied cards
         */
        var occupiedCardHeuristic = OCCUPIED_CARD_WEIGHT * calculateOccupiedCardHeuristic(game);
        reward += occupiedCardHeuristic;

        /*
         * Estimated Reinforcement
         */
        var estimatedReinforcementHeuristic = ESTIMATED_REINFORCEMENT_WEIGHT * calculateEstimatedReinforcementHeuristic(game);
        reward += estimatedReinforcementHeuristic;

        /*
         * Front line troop distribution
         */
        var troopFrontLineDistributionHeuristics = TROOP_FRONT_LINE_DISTRIBUTION_WIGHT * calculateFrontLineTroopDistribution(game);
        reward += troopFrontLineDistributionHeuristics;

        /*
         * Ensures that troops are not stacked on one field
         */
        var troopSpreadHeuristic = TROOP_SPREAD_WEIGHT * calculateTroopSpreadHeuristic(game);
        reward += troopSpreadHeuristic;

        /*
         * Ensures that troops are not stacked on one field (quadratic function)
         */
        var troopSpreadQuadraticHeuristic = TROOP_SPREAD_QUADRATIC_WEIGHT * calculateTroopSpreadQuadraticHeuristic(game);
        reward += troopSpreadQuadraticHeuristic;

        /*
         * Reinforce at front line
         */
        var reinforceFrontLineHeuristic = REINFORCE_FRONT_LINE_WEIGHT * calculateReinforceFrontLineHeuristic(game);
        reward += reinforceFrontLineHeuristic;

        /*
         * Rush to the front line
         */
        var rushToFrontLine = rushForwardWeight * calculateRushForwardHeuristic(game);
        reward += rushToFrontLine;

        // Features which should not be applies in the fortify/reinforcement phase
        var enemyOccupiedContinentsHeuristic = 0.0;
        var ownOccupiedContinentsHeuristic = 0.0;
        var continentDominationHeuristic = 0.0;
        var continentArmyDominationHeuristic = 0.0;
        if (!board.isFortifyPhase() && !board.isReinforcementPhase()) {
            /*
             * Enemy occupied continents
             */
            enemyOccupiedContinentsHeuristic = ENEMY_OCCUPIED_CONTINENTS_WEIGHT * calculateEnemyOccupiedContinentsHeuristic();
            reward += enemyOccupiedContinentsHeuristic;

            /*
             * Own occupied continents
             */
            ownOccupiedContinentsHeuristic = OWN_OCCUPIED_CONTINENTS_WEIGHT * calculateOwnOccupiedContinentsHeuristic();
            reward += ownOccupiedContinentsHeuristic;

            /*
             * Continent Domination Feature
             */
            continentDominationHeuristic = CONTINENT_DOMINATION_WEIGHT * calculateContinentDominationHeuristic();
            reward += continentDominationHeuristic;

        }
        if (!board.isAttackPhase()) {
            /*
             * Continent Army Domination Feature
             */
            continentArmyDominationHeuristic = CONTINENT_ARMY_DOMINATION_WEIGHT * calculateContinentArmyDominationHeuristic(board);
            reward += continentArmyDominationHeuristic;
        }
        /*
         * More than one army feature
         */
        var moreThanOneArmyHeuristic = MORE_THAN_ONE_ARMY_WEIGHT * calculateMoreThanOneArmyHeuristic(board);
        reward += moreThanOneArmyHeuristic;

        /*
         * Troop distribution feature
         */
        var troopDistributionHeuristic = TROOP_DISTRIBUTION_WEIGHT * calculateTroopDistributionHeuristic(board);
        reward += troopDistributionHeuristic;

        return reward;
    }

    /**
     * Phases of the game:
     * -) reinforcement (trade in cards and place armies) ->
     * -) attack ->
     * -) occupy (place armies after attack) ->
     * -) fortify (move armies)
     */
    public enum GamePhase {
        REINFORCEMENT_PHASE("reinforcement", 0),
        OCCUPY_PHASE("occupy", 1),
        ATTACK_PHASE("attack", 2),
        FORTIFY_PHASE("fortify", 3);

        private final String name;
        private final Integer id;

        GamePhase(String name, Integer id) {
            this.name = name;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public Integer getId() {
            return id;
        }
    }
}

