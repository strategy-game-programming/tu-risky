package tu.risky.agents.risk.alpha_beta;

import at.ac.tuwien.ifs.sge.game.Game;
import at.ac.tuwien.ifs.sge.game.risk.board.RiskAction;
import at.ac.tuwien.ifs.sge.util.node.GameNode;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class RiskAbGameNode implements GameNode<RiskAction> {
    private final Map<RiskAction, Integer> actionFrequency;
    private Game<RiskAction, ?> game;
    private double utility;
    private double heuristic;
    private int absoluteDepth;
    private boolean evaluated;
    public RiskAction action;


    public RiskAbGameNode() {
        this(null, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, 0);
    }

    public RiskAbGameNode(Game<RiskAction, ?> game, RiskAction action, int absoluteDepth) {
        this(game.doAction(action), absoluteDepth);
        this.action = action;
    }

    public RiskAbGameNode(Game<RiskAction, ?> game, int absoluteDepth) {
        this(game, Double.NEGATIVE_INFINITY * (
                0 <= game.getCurrentPlayer() && game.getCurrentPlayer() < game.getNumberOfPlayers()
                        ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY
        ), Double.NEGATIVE_INFINITY * (
                0 <= game.getCurrentPlayer() && game.getCurrentPlayer() < game.getNumberOfPlayers()
                        ? Double.POSITIVE_INFINITY : Double.NEGATIVE_INFINITY
        ), absoluteDepth);
    }

    public RiskAbGameNode(Game<RiskAction, ?> game, double utility, double heuristic, int absoluteDepth) {
        this.game = game;
        this.utility = utility;
        this.heuristic = heuristic;
        this.absoluteDepth = absoluteDepth;
        this.evaluated = false;
        if (game != null && game.getCurrentPlayer() < 0) {
            this.actionFrequency = new HashMap<>();
        } else {
            this.actionFrequency = null;
        }

    }

    public Game<RiskAction, ?> getGame() {
        return this.game;
    }

    public void setGame(Game<RiskAction, ?> game) {
        this.game = game;
    }

    public double getUtility() {
        return this.utility;
    }

    public void setUtility(double utility) {
        this.utility = utility;
    }

    public double getHeuristic() {
        return this.heuristic;
    }

    public void setHeuristic(double heuristic) {
        this.heuristic = heuristic;
    }

    public int getAbsoluteDepth() {
        return this.absoluteDepth;
    }

    public void setAbsoluteDepth(int absoluteDepth) {
        this.absoluteDepth = absoluteDepth;
    }

    public boolean isEvaluated() {
        return this.evaluated;
    }

    public void setEvaluated(boolean evaluated) {
        this.evaluated = evaluated;
    }

    public void simulateDetermineAction() {
        var action = this.game.determineNextAction();
        if (action != null && this.actionFrequency != null) {
            this.actionFrequency
                    .compute(action, (k, v) -> v == null ? 1 : v + 1);
        }

    }

    public void simulateDetermineAction(int times) {
        IntStream.range(0, times)
                .forEach(i -> this.simulateDetermineAction());
    }

    public boolean areSimulationDone() {
        return this.actionFrequency != null && !this.actionFrequency.isEmpty();
    }

    public int simulationsDone() {
        return this.actionFrequency == null ? 0 : this.actionFrequency.values()
                .stream()
                .reduce(0, Integer::sum);
    }

    public double frequencyOf(RiskAction action) {
        if (this.actionFrequency == null) {
            return 1.0D;
        } else {
            double all = this.simulationsDone();
            double n = (double) this.actionFrequency.getOrDefault(action, 0);
            return n / all;
        }
    }

    public RiskAction mostFrequentAction() {
        RiskAction action = null;
        if (this.actionFrequency != null) {
            var frequency = this.actionFrequency.entrySet()
                    .stream()
                    .max(Comparator.comparingInt(Map.Entry::getValue))
                    .orElse(null);
            if (frequency != null) {
                action = frequency.getKey();
            }
        }
        return action;
    }

    public boolean isMostFrequentAction(RiskAction action) {
        if (this.actionFrequency != null) {
            int frequency = this.actionFrequency.getOrDefault(action, 0);
            return this.actionFrequency.values()
                    .stream()
                    .noneMatch(f -> f > frequency);
        } else {
            return false;
        }
    }
}

