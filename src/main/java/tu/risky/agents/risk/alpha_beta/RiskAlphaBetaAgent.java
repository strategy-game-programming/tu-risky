package tu.risky.agents.risk.alpha_beta;

import at.ac.tuwien.ifs.sge.agent.AbstractGameAgent;
import at.ac.tuwien.ifs.sge.agent.GameAgent;
import at.ac.tuwien.ifs.sge.engine.Logger;
import at.ac.tuwien.ifs.sge.game.Game;
import at.ac.tuwien.ifs.sge.game.risk.board.Risk;
import at.ac.tuwien.ifs.sge.game.risk.board.RiskAction;
import at.ac.tuwien.ifs.sge.game.risk.board.RiskBoard;
import at.ac.tuwien.ifs.sge.util.Util;
import at.ac.tuwien.ifs.sge.util.tree.DoubleLinkedTree;
import at.ac.tuwien.ifs.sge.util.tree.Tree;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * An agent that calculates the next best move with alpha-beta pruning search algorithm.
 * For enhanced performance:
 * - cleaned up code
 * - different heuristic
 * <p>
 * The base structure of this class is from the Alpha-Beta Agent provided with the course material.
 * We made several adjustments and optimized the performance of the agent.
 */
public class RiskAlphaBetaAgent extends AbstractGameAgent<Risk, RiskAction> implements
        GameAgent<Risk, RiskAction> {
    private static final int MAX_LABEL_DEPTH_COUNT = 32;
    private static final int MAX_DEPTH_DEFAULT = 64;
    private static final double RANDOM_DROP_FACTOR = 0.05;
    // simulation time factors
    private static final double SIMULATION_TIME_FACTOR = 21.9815d;
    private static final double SIMULATION_TIME_LOG_FACTOR = 1.57606d;
    // branching factors
    private static final double BRANCHING_FACTOR = 11.9581d;
    private static final double BRANCHING_LOG_FACTOR = -0.0447066d;
    // time factor
    private static final double TIME_FACTOR = 0.360674d;
    private static final double TIME_LOG_FACTOR = 0.4d;
    // excess time factor
    private static final double EXCESS_TIME_FACTOR = 1.11622d;
    private static final double EXCESS_TIME_LOG_FACTOR = 1.22474d;
    // down scaling factor
    private static final double TREE_BRANCH_GROUP_FACTOR = 0.2d;

    private int lastDepth;
    private int depth;
    private Comparator<RiskAbGameNode> gameAbNodeEvaluatedComparator;
    private Comparator<RiskAbGameNode> gameAbNodeComparator;
    private Comparator<RiskAbGameNode> gameAbNodeMoveComparator;
    private Comparator<Tree<RiskAbGameNode>> gameAbTreeComparator;
    private final Tree<RiskAbGameNode> abTree;
    private int alphaCutOffs;
    private int betaCutOffs;
    private int excessTime;
    private int averageBranchingCount;
    private double averageBranching;
    private File resFile;
    private Random r = new Random();

    private RiskHeuristic riskHeuristic = null;

    /********************************************************************************************
     * Constrcutors
     ********************************************************************************************/

    public RiskAlphaBetaAgent() {
        this(null);
    }

    public RiskAlphaBetaAgent(Logger log) {
        super(log);
        abTree = new DoubleLinkedTree<>();
    }


    /***********************************************************************************************
     * Setup and game init
     ***********************************************************************************************/

    @Override
    public void setUp(int numberOfPlayers, int playerId) {
        super.setUp(numberOfPlayers, playerId);
        abTree.clear();
        abTree.setNode(new RiskAbGameNode());
        averageBranchingCount = 0;
        averageBranching = 10.0D;
        Comparator<RiskAbGameNode> gameAbNodeUtilityComparator = Comparator.comparingDouble(RiskAbGameNode::getUtility);
        Comparator<RiskAbGameNode> gameAbNodeHeuristicComparator = Comparator.comparingDouble(RiskAbGameNode::getHeuristic);
        gameAbNodeEvaluatedComparator = (o1, o2) -> Boolean.compare(o1.isEvaluated(), o2.isEvaluated());
        gameAbNodeComparator = gameAbNodeUtilityComparator.thenComparing(gameAbNodeHeuristicComparator);
        gameAbNodeMoveComparator = gameAbNodeComparator.thenComparing((o1, o2) ->
                gameComparator.compare(o1.getGame(), o2.getGame()));
        gameAbTreeComparator = Comparator.comparing(Tree<RiskAbGameNode>::getNode, (o1, o2) ->
                gameAbNodeEvaluatedComparator.compare(o1, o2)
        ).thenComparing((o1, o2) ->
                gameAbNodeMoveComparator.compare(o1.getNode(), o2.getNode())
        );
    }

    /****************************************************************************************************
     *  Compute action / heuristic
     ****************************************************************************************************/

    @Override
    public RiskAction computeNextAction(Risk game, long computationTime, TimeUnit timeUnit) {
        try {
            if (riskHeuristic == null) {
                riskHeuristic = new RiskHeuristic(game.getBoard(), playerId, log);
                resFile = new File(String.format("%s/%d_state", RiskHeuristic.OUT_PATH, riskHeuristic.timeStamp));
            }
            super.setTimers(computationTime, timeUnit);
            Util.findRoot(abTree, game); // re root game tree to current game state

            log.tra_("Check if best move will eventually end game: ");
            if (sortPromisingCandidates(abTree, gameAbNodeComparator.reversed())) {
                Tree<RiskAbGameNode> maxTreeNode = Collections.max(abTree.getChildren(), gameAbTreeComparator);
                var selectedAction = maxTreeNode.getNode().getGame().getPreviousAction();
                tryWriteGameStateToFile(maxTreeNode.getNode().getGame());
                logAction(selectedAction, abTree.getNode(), (RiskBoard) abTree.getNode().getGame().getBoard());
                return selectedAction;
            } else {
                lastDepth = 1;
                excessTime = 2;
                int labeled = 1;

                while (!shouldStopComputation() && excessTime > 1 && labeled <= lastDepth) {
                    depth = determineDepth();
                    alphaCutOffs = 0;
                    betaCutOffs = 0;
                    labelAlphaBetaTree(abTree, depth);
                    excessTime = (int) (TIMEOUT / Math.min(Math.max(System.nanoTime() - START_TIME, 1L), TIMEOUT));
                    ++labeled;
                }

                log._debugf(", done with %d alpha cut-off%s, %d beta cut-off%s and %s left.", alphaCutOffs, alphaCutOffs != 1 ? "s" : "", betaCutOffs, betaCutOffs != 1 ? "s" : "", Util.convertUnitToReadableString(ACTUAL_TIMEOUT - (System.nanoTime() - START_TIME), TimeUnit.NANOSECONDS, timeUnit));
                log.tracef("Tree has %d nodes, maximum depth %d, and an average branching factor of %s", abTree.size(), depth, Util.convertDoubleToMinimalString(averageBranching, 2));
                if (abTree.isLeaf()) {
                    log.debug("Could not find a move, choosing the next best greedy option.");
                    var selectedAction = Collections.max(game.getPossibleActions(), (o1, o2) ->
                            gameComparator.compare(game.doAction(o1), game.doAction(o2)));
                    tryWriteGameStateToFile(abTree.getNode().getGame());
                    logAction(selectedAction, abTree.getNode(), (RiskBoard) abTree.getNode().getGame().getBoard());
                    return selectedAction;
                } else {
                    if (!(abTree.getNode()).isEvaluated()) {
                        labelMinMaxTree(abTree, 1);
                    }

                    log.debugf("Utility: %.1f, Heuristic: %.1f", (abTree.getNode()).getUtility(), abTree.getNode().getHeuristic());
                    Tree<RiskAbGameNode> maxTreeNode = Collections.max(abTree.getChildren(), gameAbTreeComparator);
                    var selectedAction = maxTreeNode.getNode().getGame().getPreviousAction();
                    tryWriteGameStateToFile(maxTreeNode.getNode().getGame());
                    logAction(selectedAction, abTree.getNode(), (RiskBoard) maxTreeNode.getNode().getGame().getBoard());
                    return selectedAction;
                }
            }
        } catch (Exception e) {
            if (RiskHeuristic.SAVE_OUTPUT) {
                var exFile = new File(String.format("%s/%d_exception", RiskHeuristic.OUT_PATH, riskHeuristic.timeStamp));
                var res = e.getMessage() + "\n";
                try (var writer = new FileWriter(exFile)) {
                    writer.write(res);
                    try (var pw = new PrintWriter(writer)) {
                        e.printStackTrace(pw);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            var action = game.getPossibleActions().stream().findFirst();
            return action.get();
        }
    }

    private void tryWriteGameStateToFile(Game<RiskAction, ?> game) {
        if (RiskHeuristic.SAVE_OUTPUT) {
            var res = game.toTextRepresentation();
            try (var writer = new FileWriter(resFile)) {
                writer.write(res);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void logAction(RiskAction action, RiskAbGameNode node, RiskBoard board) {
        if (RiskHeuristic.LOG_ACTIONS) {
            System.out.println("Current Phase: " + RiskHeuristic.getGamePhase(board));
            System.out.println("Selected Action: " + action.toString());
            System.out.println("Heuristic value: " + node.getHeuristic());
        }
    }

    /**
     * create a child for each possible move from the current game state and add them to the tree
     *
     * @param tree tree to which possible moves should be added as children
     * @implNote averageBranching is updated
     */
    private boolean expandNode(Tree<RiskAbGameNode> tree) {
        if (tree.isLeaf()) {
            var abGameNode = tree.getNode();
            var game = abGameNode.getGame();
            if (!game.isGameOver()) {
                var possibleActions = game.getPossibleActions();
                // reduce possible actions to bulks to reduce tree size (e.g. attack 1 or 2 troops makes little difference
                var possibleAttackOrFortifyActions = new HashSet<RiskAction>();
                var possibleReinforcementActions = new HashSet<RiskAction>();
                var otherPossibleActions = new HashSet<RiskAction>();
                groupActions(game, possibleActions, possibleAttackOrFortifyActions, possibleReinforcementActions, otherPossibleActions);
                var filteredPossibleAttackOrFortifyActions = new HashSet<RiskAction>();
                var filteredPossibleReinforcementActions = new HashSet<RiskAction>();
                filterAttackAndFortifyActions(possibleAttackOrFortifyActions, filteredPossibleAttackOrFortifyActions, game);
                filterReinforcementActions(possibleReinforcementActions, filteredPossibleReinforcementActions);
                if (RANDOM_DROP_FACTOR > r.nextDouble()) {
                    if (!filteredPossibleAttackOrFortifyActions.isEmpty()) {
                        filteredPossibleAttackOrFortifyActions.remove(filteredPossibleAttackOrFortifyActions.stream().findFirst().get());
                    }
                    if (!filteredPossibleReinforcementActions.isEmpty()) {
                        filteredPossibleReinforcementActions.remove(filteredPossibleReinforcementActions.stream().findFirst().get());
                    }
                }
                // calculate average branching
                averageBranching = (averageBranching * averageBranchingCount++ + possibleActions.size()) / averageBranchingCount;
                filteredPossibleAttackOrFortifyActions.forEach(it ->
                        tree.add(new RiskAbGameNode(game, it, abGameNode.getAbsoluteDepth() + 1))
                );
                filteredPossibleReinforcementActions.forEach(it ->
                        tree.add(new RiskAbGameNode(game, it, abGameNode.getAbsoluteDepth() + 1))
                );
                otherPossibleActions.forEach(it ->
                        tree.add(new RiskAbGameNode(game, it, abGameNode.getAbsoluteDepth() + 1))
                );
            }
        }
        return !tree.isLeaf();
    }

    private void groupActions(Game<RiskAction, ?> game, Set<RiskAction> possibleActions, HashSet<RiskAction> possibleAttackOrFortifyActions, HashSet<RiskAction> possibleReinforcementActions, HashSet<RiskAction> otherPossibleActions) {
        possibleActions
                .forEach(it -> {
                    var resultGame = (Risk) game.doAction(it);
                    switch (Objects.requireNonNull(RiskHeuristic.getGamePhase(resultGame.getBoard()))) {
                        case ATTACK_PHASE:
                        case FORTIFY_PHASE:
                            possibleAttackOrFortifyActions.add(it);
                            break;
                        case REINFORCEMENT_PHASE:
                            possibleReinforcementActions.add(it);
                            break;
                        default:
                            otherPossibleActions.add(it);
                    }
                });
    }

    private void filterReinforcementActions(HashSet<RiskAction> possibleReinforcementActions, HashSet<RiskAction> filteredPossibleReinforcementActions) {
        if (!possibleReinforcementActions.isEmpty()) {
            var groupedReinforcementActions =
                    possibleReinforcementActions
                            .stream()
                            .collect(Collectors.groupingBy(RiskAction::reinforcedId, Collectors.toSet()));
            for (var key : groupedReinforcementActions.keySet()) {
                var sortedActions = groupedReinforcementActions.get(key)
                        .stream()
                        .sorted(Comparator.comparing(RiskAction::troops))
                        .collect(Collectors.toList());
                int index = (int) Math.ceil(sortedActions.size() * TREE_BRANCH_GROUP_FACTOR);
                log.trace("Max size" + sortedActions.size() + "Add index 0/last");
                filteredPossibleReinforcementActions.add(sortedActions.get(0));
                if (sortedActions.size() != 1) {
                    filteredPossibleReinforcementActions.add(sortedActions.get(sortedActions.size() - 1));
                }
                while (index < (sortedActions.size() - 1)) {
                    log.trace("Adding index" + index);
                    filteredPossibleReinforcementActions.add(sortedActions.get(index));
                    index += (int) Math.ceil(sortedActions.size() * TREE_BRANCH_GROUP_FACTOR);
                }
            }
        }
    }

    private void filterAttackAndFortifyActions(HashSet<RiskAction> possibleAttackOrFortifyActions, HashSet<RiskAction> filteredPossibleAttackOrFortifyActions, Game<RiskAction, ?> game) {
        if (!possibleAttackOrFortifyActions.isEmpty()) {
            var sortedAttackOrFortifyActions = possibleAttackOrFortifyActions
                    .stream()
                    .sorted(Comparator.comparing(RiskAction::troops))
                    .collect(Collectors.toList());
            int index = (int) Math.ceil(sortedAttackOrFortifyActions.size() * TREE_BRANCH_GROUP_FACTOR);
            log.trace("Max size" + sortedAttackOrFortifyActions.size() + "Add index 0/last");
            if (sortedAttackOrFortifyActions.size() == 1) {
                filteredPossibleAttackOrFortifyActions.add(sortedAttackOrFortifyActions.get(0));
            }
            if (sortedAttackOrFortifyActions.size() != 1) {
                var lastAction = sortedAttackOrFortifyActions.get(sortedAttackOrFortifyActions.size() - 1);
                if (((RiskBoard) game.getBoard()).isAttackPhase()) {
                    filteredPossibleAttackOrFortifyActions.add(lastAction);
                }
            }
            while (index < (sortedAttackOrFortifyActions.size() - 1)) {
                log.trace("Adding index" + index);
                filteredPossibleAttackOrFortifyActions.add(sortedAttackOrFortifyActions.get(index));
                index += (int) Math.ceil(sortedAttackOrFortifyActions.size() * TREE_BRANCH_GROUP_FACTOR);
            }
        }
    }

    private boolean appearsQuiet(Tree<RiskAbGameNode> tree) {
        if (tree.isRoot()) {
            return true;
        } else {
            var siblings = tree.getParent().getChildren();
            double min = Collections.min(siblings, gameAbTreeComparator).getNode().getUtility();
            double max = Collections.max(siblings, gameAbTreeComparator).getNode().getUtility();
            return siblings.size() <= 2 || min < tree.getNode().getGame().getUtilityValue() && tree.getNode().getGame().getUtilityValue() < max;
        }
    }

    private void quiescence(Tree<RiskAbGameNode> tree) {
        boolean isQuiet = false;
        RiskAbGameNode node;
        for (node = tree.getNode(); !node.isEvaluated(); node = tree.getNode()) {
            var game = node.getGame();
            if (!game.isGameOver() && (game.getCurrentPlayer() < 0 || !isQuiet && !appearsQuiet(tree))) {
                expandNode(tree);
                tree.sort(gameAbNodeComparator);
                var size = tree.getChildren().size();
                if (size != 0) {
                    tree = tree.getChild(size / 2);
                }
                isQuiet = true;
            } else {
                double heuristicValue = calculateHeuristic((Game<RiskAction, RiskBoard>) game);
                double utilityValue = calculateUtilityValue((Game<RiskAction, RiskBoard>) game);
                node.setUtility(utilityValue);
                node.setHeuristic(heuristicValue);
                node.setEvaluated(true);
            }
        }

        var originalNode = tree.getNode();
        if (!originalNode.isEvaluated()) {
            originalNode.setUtility(node.getUtility());
            originalNode.setHeuristic(node.getHeuristic());
            originalNode.setEvaluated(true);
        }
    }

    private double calculateUtilityValue(Game<RiskAction, RiskBoard> game) {
        return riskHeuristic.calculateUtilityValue(game);
    }

    /**
     * Get the heuristic for the given node
     */
    private double calculateHeuristic(at.ac.tuwien.ifs.sge.game.Game<RiskAction, RiskBoard> game) {
        return riskHeuristic.calculateHeuristic(game);
    }

    /**
     * Propagate value form not up to parent
     *
     * @param tree tree containing considered game nodes
     */
    private void evaluateNode(Tree<RiskAbGameNode> tree) {
        var node = tree.getNode();
        if (tree.isLeaf()) {
            quiescence(tree);
        }

        if (!tree.isRoot()) {
            var parent = tree.getParent().getNode();
            int parentCurrentPlayer = parent.getGame().getCurrentPlayer();
            double utility = node.getUtility();
            double heuristic = node.getHeuristic();
            if (!parent.isEvaluated()) {
                parent.setUtility(utility);
                parent.setHeuristic(heuristic);
            } else if (parentCurrentPlayer < 0) {
                int nrOfSiblings = tree.getParent().getChildren().size();
                if (!parent.areSimulationDone()) {
                    parent.simulateDetermineAction(Math.max((int) Math.round(nrOfSiblings * simulationTimeFactor()), nrOfSiblings));
                }

                parent.simulateDetermineAction(nrOfSiblings);
                if (parent.isMostFrequentAction(node.getGame().getPreviousAction())) {
                    parent.setUtility(utility);
                    parent.setHeuristic(heuristic);
                }
            } else {
                double parentUtility = parent.getUtility();
                double parentHeuristic = parent.getHeuristic();
                if (parentCurrentPlayer == playerId) {
                    parent.setUtility(Math.max(parentUtility, utility));
                    parent.setHeuristic(Math.max(parentHeuristic, heuristic));
                } else {
                    parent.setUtility(Math.min(parentUtility, utility));
                    parent.setHeuristic(Math.min(parentHeuristic, heuristic));
                }
            }

            parent.setEvaluated(true);
        }

    }

    private void labelMinMaxTree(Tree<RiskAbGameNode> tree, int depth) {
        var stack = new ArrayDeque<Tree<RiskAbGameNode>>();
        stack.push(tree);
        Tree<RiskAbGameNode> lastParent = null;
        depth = Math.max((tree.getNode()).getAbsoluteDepth() + depth, depth);
        int labelCount = 0;
        while (!stack.isEmpty() && !shouldStopComputation()) {
            if (labelCount >= MAX_LABEL_DEPTH_COUNT)
                break;
            labelCount++;
            tree = stack.peek();
            assert tree != null;
            if (lastParent != tree && tree.getNode().getAbsoluteDepth() < depth && expandNode(tree)) {
                pushChildrenOntoStack(tree, stack);
            } else {
                evaluateNode(tree);
                stack.pop();
                lastParent = tree.getParent();
            }
        }
    }

    /**
     * Sorts the tree considering the utility values and the current player and pushes them in order onto the stack.
     * Sorts the tree depending on the current player, if this agent is the current player the tree is sorted (highest
     * utility first), if the current player is the enemy the tree is sorted in reverse (lowest utility first).
     * All children of the sorted tree are pushed onto the stack.
     *
     * @param tree  tree which should be sorted
     * @param stack stack with children in sorted order
     */
    private void pushChildrenOntoStack(Tree<RiskAbGameNode> tree, Deque<Tree<RiskAbGameNode>> stack) {
        if (tree.getNode().getGame().getCurrentPlayer() == playerId) {
            tree.sort(gameAbNodeMoveComparator);
        } else {
            tree.sort(gameAbNodeMoveComparator.reversed());
        }
        tree.getChildren().forEach(stack::push);
    }

    /**
     * Alpha = lower bound on score of the node, it is never greater than the ture score of the node.
     * at a max-node (current player is this player) it is set to largest score of it explored children
     * at a min-node it is set to the value of it's parent
     * Beta = upper bound on score of the node, it is never smaller than the ture score of the node.
     * at max-node it is set to the value of it's parent
     * at min-node it is set to smalles value of it's  explored children
     */
    private void labelAlphaBetaTree(Tree<RiskAbGameNode> tree, int depth) {
        var utilityAlpha = Double.NEGATIVE_INFINITY;
        var utilityBeta = Double.POSITIVE_INFINITY;
        var heuristicAlpha = Double.NEGATIVE_INFINITY;
        var heuristicBeta = Double.POSITIVE_INFINITY;
        var stack = new ArrayDeque<Tree<RiskAbGameNode>>();
        var utilityAlphas = new ArrayDeque<Double>();
        var heuristicAlphas = new ArrayDeque<Double>();
        var utilityBetas = new ArrayDeque<Double>();
        var heuristicBetas = new ArrayDeque<Double>();
        stack.push(tree);
        utilityAlphas.push(utilityAlpha);
        utilityBetas.push(utilityBeta);
        heuristicAlphas.push(heuristicAlpha);
        heuristicBetas.push(heuristicBeta);

        depth = Math.max(tree.getNode().getAbsoluteDepth() + depth, depth);

        Tree<RiskAbGameNode> lastParent = null;

        int checkDepth = 0;
        while (!stack.isEmpty() && (checkDepth++ % 31 != 0 || !shouldStopComputation())) {

            tree = stack.peek();

            assert tree != null;
            if (lastParent == tree || tree.getNode().getAbsoluteDepth() >= depth || !expandNode(tree)) {
                evaluateNode(tree);

                if (tree.isRoot()
                        || tree.getParent().getNode().getGame().getCurrentPlayer() == playerId) {
                    utilityAlpha = Math.max(Objects.requireNonNull(utilityAlphas.peek()), tree.getNode().getUtility());
                    heuristicAlpha = Math.max(Objects.requireNonNull(heuristicAlphas.peek()), tree.getNode().getHeuristic());
                    utilityBeta = Objects.requireNonNull(utilityBetas.peek());
                    heuristicBeta = Objects.requireNonNull(heuristicBetas.peek());
                } else {
                    utilityAlpha = Objects.requireNonNull(utilityAlphas.peek());
                    heuristicAlpha = Objects.requireNonNull(heuristicAlphas.peek());
                    utilityBeta = Math.min(Objects.requireNonNull(utilityBetas.peek()), tree.getNode().getUtility());
                    heuristicBeta = Math.min(Objects.requireNonNull(heuristicBetas.peek()), tree.getNode().getUtility());
                }

                stack.pop();
                if (lastParent == tree) {
                    utilityAlphas.pop();
                    utilityBetas.pop();
                    heuristicAlphas.pop();
                    heuristicBetas.pop();
                }

                lastParent = tree.getParent();
            } else if ((utilityAlpha < utilityBeta && heuristicAlpha < heuristicBeta) || (
                    tree.getParent() != null
                            && tree.getParent().getNode().getGame().getCurrentPlayer() < 0)) {
                pushChildrenOntoStack(tree, stack);
                utilityAlphas.push(utilityAlpha);
                heuristicAlphas.push(heuristicAlpha);
                utilityBetas.push(utilityBeta);
                heuristicBetas.push(heuristicBeta);
            } else if (tree.getParent() != null
                    && tree.getParent().getNode().getGame().getCurrentPlayer() >= 0) {
                if (tree.isRoot()
                        || tree.getParent().getNode().getGame().getCurrentPlayer() == playerId) {
                    betaCutOffs++;
                } else {
                    alphaCutOffs++;
                }
                tree.getNode().setEvaluated(false);
                tree.dropChildren();
                stack.pop();
            }
        }
    }


    /**
     * estimate a factor to consider how much time is left in the simulation of the next move
     */
    private double simulationTimeFactor() {
        return SIMULATION_TIME_FACTOR * Math.log(SIMULATION_TIME_LOG_FACTOR * (nanosLeft() / 1000000000.0));
    }

    /**
     * estimate branching factor (=number of children of each node)
     *
     * @return estimated branching factor
     */
    private double branchingFactor() {
        return BRANCHING_FACTOR * Math.exp(BRANCHING_LOG_FACTOR * averageBranching);
    }

    /**
     * estimate a time factor to consider how much time is left when calculating the depth to which the simulation will go
     *
     * @return estimated time factor for depth calculation
     */
    private double timeFactor() {
        return TIME_FACTOR * Math.log(TIME_LOG_FACTOR * (double) TimeUnit.NANOSECONDS.toSeconds(nanosLeft()));
    }

    private double excessTimeBonus() {
        return EXCESS_TIME_FACTOR * Math.log(EXCESS_TIME_LOG_FACTOR * (double) excessTime);
    }

    /**
     * calculate depth based on branching factor and time factor
     */
    private int determineDepth() {
        depth = (int) Math.max(Math.round(branchingFactor() * timeFactor()), 2L);
        depth = Math.max(lastDepth + (int) Math.round(excessTimeBonus()), depth);
        depth = Math.min(depth, MAX_DEPTH_DEFAULT);
        lastDepth = depth;
        return depth;
    }

    private boolean sortPromisingCandidates(Tree<RiskAbGameNode> tree, Comparator<RiskAbGameNode> comparator) {
        boolean isDetermined = true;
        while (!tree.isLeaf() && tree.getNode().isEvaluated() && isDetermined) {
            isDetermined = tree.getChildren()
                    .stream()
                    .allMatch(c -> c.getNode().getGame().getCurrentPlayer() >= 0);
            if (tree.getNode().getGame().getCurrentPlayer() == playerId) {
                tree
                        .sort(gameAbNodeEvaluatedComparator.reversed()
                                .thenComparing(comparator));
            } else {
                tree
                        .sort(gameAbNodeEvaluatedComparator.reversed()
                                .thenComparing(comparator.reversed()));
            }
            tree = tree.getChild(0);
        }
        return tree.getNode().isEvaluated() && tree.getNode().getGame().isGameOver();
    }

    /**
     * Name of the agent
     */
    @Override
    public String toString() {
        return "TURiskyRiskAbAgent";
    }

    /**
     * Clean up resources + free space
     */

    @Override
    public void tearDown() {
        // don't need dis yet
    }

    @Override
    public void ponderStart() {
        // don't need dis yet (maybe instad of setup)
    }

    @Override
    public void ponderStop() {
        // don't need dis yet
    }

    @Override
    public void destroy() {
        // don't need dis yet
    }
}

