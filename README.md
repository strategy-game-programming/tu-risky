Alpha-Beta-Agent
=================

Our starting point for our *Alpha-Beta-Agent* was the *Alpha-Beta-Agent* that was provided with the course material. 
We made the following adjustments:

- **Filter possible actions**
    - *Reinforcement Phase:* if there are multiple actions with the same `target_id` for the possible troops the quintiles are used
    - *Attack Phase:* if there are multiple actions with the same `src_id` and `target_id` for the troops q2 to q5 (quintiles) are used
    - *Fortify and Occupy Phase:* if there are multiple actions, for the troops q2 to q5 are used
- **Random cut off**
- **Some runtime improvements**

Heuristics
----------

We implemented several heuristics.
In a first attempt we implemented heuristics based on the insights from the  paper "An Intelligent Artifical Player for the GAme of Risk - Michale Wolf".
We adjusted most of them, in order to be better suited our implementation. We also introduced several new heuristics. 
A detailed description of our heuristics can be found in the form of *java-doc* in the file ***RiskHeuristic.java***.

Each heuristic has a weight which determines the contribution to the total reward.
We made sure, that all our heuristics are normalized, to make it easier to determine weights for each feature.

Different weight configurations
-------------------------------------

In the folder weights are different configurations of the weights.
We tested multiple weight configuration, which aimed for a balanced weight distribution.
We found, that these yielded very poor results. 
We concluded that some aspects of the game are more important than others. In order to evaluate, which
heuristics should get the highest priorities we tested weight configurations, that have a strong focus
on one aspect. Some tested configurations can be found in the folder ***weight***.

**Test with weight files**
- create ***Weights*** file in project root 
- `mkdir out/runs`
- `java -jar grassauer-pss-risk-env/sge-1.0.1-exe.jar match --file=grassauer-pss-risk-env/sge-risk-1.0.1-exe.jar --directory=grassauer-pss-risk-env/agents -q -q -a TURiskyRiskAbAgent AlphaBetaAgent </dev/null &>/dev/null &`

Results can be found in ***out/runs***.
Last game state is saved in ***\<timestamp>_state***. The used weights for the state are saved in 
***\<timestamp>_weights***. If errors occurred the exception is loged to ***\<timestamp>_exception***.

The final configuration that yields the best results is:
```
OCCUPY_TERRITORIES_WEIGHT              = 200
TROOP_STRENGTH_WEIGHT                  = 30
FRONT_LINE_WEIGHT                      = 100
ESTIMATED_REINFORCEMENT_WEIGHT         = 30
CONTINENT_SAFETY_WEIGHT                = 10
CONTINENT_THREAT_WEIGHT                = 10
ENEMY_OCCUPIED_CONTINENTS_WEIGHT       = 25
OWN_OCCUPIED_CONTINENTS_WEIGHT         = 60
CONTINENT_ARMY_DOMINATION_WEIGHT       = 10
CONTINENT_DOMINATION_WEIGHT            = 10
MORE_THAN_ONE_ARMY_WEIGHT              = 50
HINTERLAND_WEIGHT                      = 50
CARD_WEIGHT                            = 10
OCCUPIED_CARD_WEIGHT                   = 10
TROOP_DISTRIBUTION_WEIGHT              = 0
TROOP_SPREAD_WEIGHT                    = 10
TROOP_SPREAD_QUADRATIC_WEIGHT          = 100
TROOP_FRONT_LINE_DISTRIBUTION_WIGHT    = 50
REINFORCE_FRONT_LINE_WEIGHT            = 100
RUSH_FORWARD_HEURISTIC                 = 10
```

*Since a single game takes quite some time, it was hard to test the different configurations extensively.*

Neural Network Attempt:
=======================

We planed to do experiments with neural networks. 
Due to time constrains we could not finish this project.
In this section the planned architecture is described.

We agreed to use `pytorch`. 
We implemented a socket communication. This enabled us to communicate the *game state* and *actions* between
Java and Python. To represent the game state in Python we wrapped it in a custom `gym enviroment`.

**Planed network architecture:**
- input layer: game state with an additional row indicating the game phase
- output layer: 42 x 3 array
    - first row represents `src_id`
    - second row represents `target_id`
    - third row represents troops as percentage in steps of ~2.4%
- convolution and fully connected layers in between

**Handling of illegal actions:**
Do not change the game state if the network selects an illegal action. 
The idea was, that the network would over time learn that legal actions can increase the reward, 
while illegal actions can't.
  
**Actions:**
The network would predict for each move `src_id`, `target_id` and `troops`. We planed on
ignoring the fields which are not needed in a specific game phase.
